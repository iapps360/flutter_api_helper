library api;

import 'dart:async';
import 'dart:core';
import 'package:dio/dio.dart';
import 'package:connectivity/connectivity.dart';
import 'package:dio_smart_retry/dio_smart_retry.dart';
import 'package:flutter/foundation.dart';

class Api {
  static BaseOptions options = new BaseOptions(
    connectTimeout: 5000,
    receiveTimeout: 3000,
  );

  final Dio dio = new Dio(options);
  final Dio dioRetry = new Dio(options);
  final ValueNotifier<bool> isConnect = new ValueNotifier<bool>(true);

  late StreamSubscription connectivitySubscription;

  Api._() {
    // connectivity check
    connectivitySubscription = Connectivity()
        .onConnectivityChanged
        .listen((ConnectivityResult result) {
      switch (result) {
        case ConnectivityResult.mobile:
        case ConnectivityResult.wifi:
          isConnect.value = true;
          dio.unlock();
          dioRetry.unlock();
          break;
        case ConnectivityResult.none:
          isConnect.value = false;
          dio.lock();
          dioRetry.unlock();
          break;
      }
    });

    // dio.interceptors.add(RetryInterceptor(
    //   dio: dioRetry,
    // ));
    dio.interceptors.add(RetryInterceptor(
      dio: dio,
      // logPrint: print, // specify log function (optional)
      retries: 3, // retry count (optional)
      retryDelays: const [ // set delays between retries (optional)
        Duration(seconds: 1), // wait 1 sec before first retry
        Duration(seconds: 2), // wait 2 sec before second retry
        Duration(seconds: 3), // wait 3 sec before third retry
      ],
    ));
  }

  static final Api instance = Api._();

  dispose() {
    connectivitySubscription.cancel();
  }
}